## Registrations

1. Create an [Faucetpay][fpay-link] account and [link your wallets][fp-wallet]

[fpay-link]: https://bit.ly/3HIgOVp
[fp-wallet]: https://faucetpay.io/page/user-admin/linked-addresses

2. Register on every free faucet site for the script:

	- Cardano: https://bit.ly/3GbTD5O
	- BNB / BinanceCoin: https://bit.ly/32XBDgP
	- BTC: https://bit.ly/3G3TCAH
	- Dash: https://bit.ly/3JR8a9a
	- ETH: https://bit.ly/34oaJPD
	- Link: https://bit.ly/3G8a3vG
	- LTC: https://bit.ly/3n5rmGq
	- NEO: https://bit.ly/34klm60
	- Steam: https://bit.ly/3n0XH0P
	- Tron / TRX: https://bit.ly/3r38GZc
	- Tether: https://bit.ly/32RC4cO
	- USDC: https://bit.ly/3eWxdtc
	- NEM: https://bit.ly/3HLU86C
	- XRP: https://bit.ly/3n6BFtU
	- BTC: https://bit.ly/3eZRBJP
	- Doge: https://bit.ly/3t2FJ1R
	- BTC: https://bit.ly/3JJ0V33
	- LTC: https://bit.ly/3F5FdCO
	- ETH: https://bit.ly/3t6oSLJ
	- BTC: https://bit.ly/34vpuQQ
	- BNB / BinanceCoin: https://bit.ly/3HI9k4N
	- Dash: https://bit.ly/3qUOcSi
	- Doge: https://bit.ly/32XnMar
	- ETH: https://bit.ly/3zARpKD
	- LTC: https://bit.ly/3EYjVad
	- Tron / TRX: https://bit.ly/3JSfG3m
	- BTC: https://bit.ly/3f52EkQ
	- BNB / BinanceCoin: https://bit.ly/3t5V7KV
	- Dash: https://bit.ly/3JPlDOI
	- DGB: https://bit.ly/31ApyxF
	- Doge: https://bit.ly/3G6L9wH
	- ETH: https://bit.ly/3n4nk0K
	- LTC: https://bit.ly/3n4G7t1
	- Tron / TRX: https://bit.ly/3f2HgNc
	- BTC: https://bit.ly/3JPkHtO
	- BTC: https://bit.ly/31ywA5S
	- LTC: https://bit.ly/3tdBKQ2
	- BTC / BNB / BFG: https://bit.ly/3f1ZLl8
	- Doge: https://bit.ly/3G6voWy
	- Multiple: https://bit.ly/3JOrZxE
	- BTC: https://bit.ly/3HGcKoP
	- Shiba: https://bit.ly/3JNywc6

## Installation

1. Install [Tampermonkey][tmnk-link] for your Browser.
	* Optional: Install [Brave Browser][bb-link] and create a Brave Wallet, then install [Tampermonkey][tmnk-link] to it.

[tmnk-link]: https://www.tampermonkey.net/
[bb-link]: https://brave.com/


2. Install all the necessary Tampermonkey scripts:

	| Userscripts                            | Direct<br>Install |
	|----------------------------------------|:------------------:|
	| Auto Claim (Main Script)               | [install][sat-link] |
	| hCaptcha Resolver                      | [install][chsolv-link] |
	| Text Captcha Resolver                  | [install][txtsolv-link] |
	| Auto Close Windows                     | [install][clswdw-link] |
	| Focus Prevention                       | [install][fcsprev-link] |



[sat-link]: https://openuserjs.org/scripts/mesmar/[satology]_Auto_Claim_Multiple_Faucets_with_Monitor_UI
[ar-link]: https://gitlab.com/mesmar/faucet-autorolling/-/raw/main/Crypto-Autoroll-Userscript.js
[chsolv-link]: https://greasyfork.org/en/scripts/425854-hcaptcha-solver-automatically-solves-hcaptcha-in-browser
[txtsolv-link]: https://greasyfork.org/en/scripts/433536-cbl01-auto-claim-complement
[clswdw-link]: https://greasyfork.org/en/scripts/429739-closewindowbyname
[fcsprev-link]: https://greasyfork.org/en/scripts/427254-preventpagevisibility

3. Go to [Satology Website][sat-link] and you should be able to see the "Monitoring UI".
	> ![image](https://i.imgur.com/4mkQQC0.png)

4. Click on Wallet and fill in your Wallet details and save it.

5. Click on Settings and enable all the wallets you want and save it.

[sat-link]: https://satology.onrender.com/faucets/referrals

6. Get Promocodes for free rolls from [this Twitter][fauc-twitter] and fill them at the bottom of the UI into the Promo Codes field.
	> ![image](https://i.imgur.com/5KoGGWm.png)

[fauc-twitter]: http://twitter.com/cryptosfaucets




